# Category model
class Category < ApplicationRecord
  has_many :jobs
end
