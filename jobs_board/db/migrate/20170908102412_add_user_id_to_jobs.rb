# Migration to add user_id to jobs table
class AddUserIdToJobs < ActiveRecord::Migration[5.0]
  def change
    add_column :jobs, :user_id, :integer
  end
end
